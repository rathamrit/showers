package com.rathamrit.showers;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.rathamrit.showers.Adapter.WeatherListAdapter;
import com.rathamrit.showers.helpers.AppConstants;
import com.rathamrit.showers.helpers.AppHelper;
import com.rathamrit.showers.helpers.WeatherData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    private static final String CHARSET = "UTF-8";
    RecyclerView rvWeek;
    TextView tvNoInternet;

    String format = "&format=json";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instantiateWidgets();
        mainAction();

    }

    private void mainAction() {
        //Check Internet Connection
        if (AppHelper.isConnectingToInternet(MainActivity.this)) {
            rvWeek.setVisibility(View.VISIBLE);
            tvNoInternet.setVisibility(View.GONE);
            try {
                //create query string
                String url = AppConstants.ENDPOINT + URLEncoder.encode(AppConstants.QUERY, CHARSET) + format;
                new GetWeatherList().execute(url);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            tryAgainAction();
        }
    }

    private void tryAgainAction() {
        tvNoInternet.setVisibility(View.VISIBLE);
        rvWeek.setVisibility(View.GONE);
        tvNoInternet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainAction();
            }
        });
    }

    private void instantiateWidgets() {
        rvWeek = (RecyclerView) findViewById(R.id.rv_weekweather);
        tvNoInternet = (TextView) findViewById(R.id.tv_no_internet);
    }

    private JSONArray getForcasts(String s) throws JSONException {
        JSONObject responseJosn = new JSONObject(s);
        JSONObject queryJosn = responseJosn.getJSONObject(AppConstants.KEYQUERY);
        JSONObject resultJson = queryJosn.getJSONObject(AppConstants.RESULTS);
        JSONObject channelJson = resultJson.getJSONObject(AppConstants.CHANNEL);
        JSONObject itemJson = channelJson.getJSONObject(AppConstants.ITEM);
        return itemJson.getJSONArray(AppConstants.FORCASTS);
    }

    private ArrayList<WeatherData> get7DaysWeatherData(JSONArray forcastarray) throws JSONException {
        ArrayList<WeatherData> list = new ArrayList<>();
        int length = 7;
        if (forcastarray.length() < 7) {
            length = forcastarray.length();
        }
        for (int i = 0; i < length; i++) {
            JSONObject weatherObject = forcastarray.getJSONObject(i);
            String code = weatherObject.getString(AppConstants.CODE);
            String date = weatherObject.getString(AppConstants.DATE);
            String day = weatherObject.getString(AppConstants.DAY);
            String high = weatherObject.getString(AppConstants.HIGH);
            String low = weatherObject.getString(AppConstants.LOW);
            String text = weatherObject.getString(AppConstants.TEXT);
            WeatherData data = new WeatherData(code, date, day, high, low, text);
            list.add(data);
        }
        return list;
    }

    private class GetWeatherList extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle(AppConstants.DIALOGTITLE);
            dialog.setMessage(AppConstants.WEATHERLISTMSG);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {
                JSONArray forcastarray = getForcasts(s);
                //convert jsonarray to list
                ArrayList<WeatherData> list = get7DaysWeatherData(forcastarray);
                WeatherListAdapter adapter = new WeatherListAdapter(list, MainActivity.this);
                rvWeek.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                rvWeek.setAdapter(adapter);
            } catch (NullPointerException e) {
                tryAgainAction();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                URL myUrl = new URL(params[0]);
                HttpsURLConnection connection = (HttpsURLConnection) myUrl.openConnection();
                connection.setDoInput(true);

                return AppHelper.convertToString(connection.getInputStream());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
