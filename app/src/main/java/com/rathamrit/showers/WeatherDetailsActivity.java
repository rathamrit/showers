package com.rathamrit.showers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.rathamrit.showers.helpers.AppConstants;
import com.rathamrit.showers.helpers.AppHelper;
import com.rathamrit.showers.helpers.WeatherData;

/**
 * Created by suyogcomputech on 20/05/16.
 */
public class WeatherDetailsActivity extends AppCompatActivity {
    TextView tvText, tvDay, tvLow, tvHigh;
    ImageView imgText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        instantiateAllWidgets();
        WeatherData data = getIntent().getExtras().getParcelable(AppConstants.WEATHER);
        setDataToWidgets(data);
    }

    private void setDataToWidgets(WeatherData data) {
        getSupportActionBar().setTitle(data.getDate());
        tvDay.setText(data.getDay());
        tvText.setText(data.getText());
        tvLow.setText(data.getLow() + AppConstants.TEMPUNIT);
        tvHigh.setText(data.getHigh() + AppConstants.TEMPUNIT);
        imgText.setImageResource(AppHelper.getDrawable(WeatherDetailsActivity.this, data.getText()));
    }

    private void instantiateAllWidgets() {
        tvDay = (TextView) findViewById(R.id.tv_weather_day);
        tvText = (TextView) findViewById(R.id.tv_weather_text);
        tvHigh = (TextView) findViewById(R.id.tv_high);
        tvLow = (TextView) findViewById(R.id.tv_low);
        imgText = (ImageView) findViewById(R.id.img_text);
    }
}
