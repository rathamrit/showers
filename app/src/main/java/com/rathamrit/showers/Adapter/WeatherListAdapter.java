package com.rathamrit.showers.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rathamrit.showers.R;
import com.rathamrit.showers.WeatherDetailsActivity;
import com.rathamrit.showers.helpers.AppConstants;
import com.rathamrit.showers.helpers.AppHelper;
import com.rathamrit.showers.helpers.WeatherData;

import java.util.ArrayList;

/**
 * Created by suyogcomputech on 20/05/16.
 */
public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ViewHolder> {
    ArrayList<WeatherData> list;
    Context mContext;

    public WeatherListAdapter(ArrayList<WeatherData> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_weather, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final WeatherData data = list.get(position);
        holder.tvDay.setText(data.getDay());
        holder.tvDate.setText(data.getDate());
        holder.tvText.setText(data.getText());
        int img = AppHelper.getDrawable(mContext,data.getText());

        holder.imgText.setImageResource(img);

        holder.cvWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, WeatherDetailsActivity.class);
                i.putExtra(AppConstants.WEATHER, data);
                mContext.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cvWeather;
        TextView tvDate, tvDay, tvText;
        ImageView imgText;

        public ViewHolder(View itemView) {
            super(itemView);
            cvWeather = (CardView) itemView.findViewById(R.id.cv_weather);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvDay = (TextView) itemView.findViewById(R.id.tv_day);
            tvText = (TextView) itemView.findViewById(R.id.tv_text);
            imgText = (ImageView) itemView.findViewById(R.id.img_text);
        }
    }
}
