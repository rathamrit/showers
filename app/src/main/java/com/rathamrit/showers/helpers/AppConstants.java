package com.rathamrit.showers.helpers;

/**
 * Created by suyogcomputech on 20/05/16.
 */
public class AppConstants {

    public static final String DIALOGTITLE = "Please Wait";
    public static final String  WEATHERLISTMSG = "Requesting Weather Details";
    public static final String ENDPOINT = "https://query.yahooapis.com/v1/public/yql?q=";
    public static final String QUERY = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"bhubaneswar,odisha,india\")";
    public static final String KEYQUERY = "query";
    public static final String RESULTS = "results";
    public static final String CHANNEL = "channel";
    public static final String ITEM = "item";
    public static final String FORCASTS = "forecast";
    public static final String CODE = "code";
    public static final String DAY = "day";
    public static final String HIGH = "high";
    public static final String LOW = "low";
    public static final String TEXT = "text";
    public static final String DATE = "date";
    public static final String WEATHER = "weather";
    public static final String TEMPUNIT = "F";
}
