package com.rathamrit.showers.helpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by suyogcomputech on 20/05/16.
 */
public class WeatherData implements Parcelable {
    public static final Creator<WeatherData> CREATOR = new Creator<WeatherData>() {
        @Override
        public WeatherData createFromParcel(Parcel in) {
            return new WeatherData(in);
        }

        @Override
        public WeatherData[] newArray(int size) {
            return new WeatherData[size];
        }
    };
    String code, date, day, high, low, text;

    public WeatherData(String code, String date, String day, String high, String low, String text) {
        this.code = code;
        this.date = date;
        this.day = day;
        this.high = high;
        this.low = low;
        this.text = text;
    }

    protected WeatherData(Parcel in) {
        code = in.readString();
        date = in.readString();
        day = in.readString();
        high = in.readString();
        low = in.readString();
        text = in.readString();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(date);
        dest.writeString(day);
        dest.writeString(high);
        dest.writeString(low);
        dest.writeString(text);
    }
}
